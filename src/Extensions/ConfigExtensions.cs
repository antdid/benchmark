﻿using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Diagnosers;
using BenchmarkDotNet.Exporters;

namespace benchmark.Extensions
{
    public static class ConfigExtensions
    {
        public static IConfig AddConfig(this IConfig config)
        {
            return config
                .AddDiagnoser(MemoryDiagnoser.Default)
                .AddExporter(RPlotExporter.Default)
                .AddColumn(StatisticColumn.Min, StatisticColumn.Max);
        }
    }
}
