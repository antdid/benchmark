﻿using benchmark.Extensions;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Running;

namespace benchmark
{
    class Program
    {
        static void Main(string[] args)
    => BenchmarkSwitcher
        .FromAssembly(typeof(Program).Assembly)
        .Run(args,
#if DEBUG
        new DebugInProcessConfig().AddConfig()
#else
        GetGlobalConfig()
#endif
);

        static IConfig GetGlobalConfig() => DefaultConfig.Instance.AddConfig();

    }
}
