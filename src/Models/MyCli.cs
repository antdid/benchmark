namespace benchmark.Models
{
    public class MyCli
    {
        public required string Name { get; set; }
        public required string ToolCommandName { get; set; }
        public required string Description { get; set; }
        public required string Version { get; set; }
        public required string Company { get; set; }
    }
}