using System.Reflection;
using benchmark.Models;
using BenchmarkDotNet.Attributes;

namespace benchmark.Samples
{
    [InProcessAttribute]
    public class ReflectionVsSourceGenerator
    {
        [Benchmark(Baseline = true)]
        public MyCli Reflection()
        {
            var assembly = Assembly.GetEntryAssembly();
            return new MyCli
            {
                Name = assembly!.GetName().Name!,
                Description = assembly.GetCustomAttribute<AssemblyDescriptionAttribute>()!.Description,
                Version = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>()!.InformationalVersion,
                ToolCommandName = "not available",
                Company = assembly.GetCustomAttribute<AssemblyCompanyAttribute>()!.Company,
            };
        }

        [Benchmark]
        public MyCli SourceGenerator()
        {
            return new MyCli
            {
                Name = ThisCli.Name,
                Description = ThisCli.Description,
                Version = ThisCli.Version,
                ToolCommandName = ThisCli.ToolCommandName,
                Company = ThisCli.Company
            };
        }
    }
}
