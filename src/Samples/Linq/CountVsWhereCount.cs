﻿using BenchmarkDotNet.Attributes;

namespace benchmark.Samples.Linq
{
    public class CountVsWhereCount : LinqBase
    {
        [Benchmark]
        public int Where()
        {
            return _data.Where(u => u.Name == _name).Count();
        }

        [Benchmark(Baseline = true)]
        public int Count()
        {
            return _data.Count(u => u.Name == _name);
        }
    }
}
