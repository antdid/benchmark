﻿using benchmark.Models;
using BenchmarkDotNet.Attributes;

namespace benchmark.Samples.Linq
{
    public abstract class LinqBase
    {
        protected readonly List<User> _data = new();
        protected string? _name;

        [Params(100, 1000, 10_000, 100_000)]
        public int N;

        [GlobalSetup]
        public void Setup()
        {
            for (var i = 0; i < N; i++)
            {
                _data.Add(new User { Name = "name" + i });
            }
            _name = "name" + (N / 4 * 3);
        }
    }
}
