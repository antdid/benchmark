﻿using benchmark.Models;
using BenchmarkDotNet.Attributes;

namespace benchmark.Samples.Linq
{
    public class FirstOrDefaultVsFind : LinqBase
    {
        [Benchmark(Baseline = true)]
        public User? FirstOrDefault()
        {
            return _data.FirstOrDefault(u => u.Name == _name);
        }

        [Benchmark]
        public User? Find()
        {
            return _data.Find(u => u.Name == _name);
        }
    }
}
