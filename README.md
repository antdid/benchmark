# Benchmark

Collection of benchmarks running via [BenchmarkDotNet](https://benchmarkdotnet.org/index.html)

## Installation

Download the [.NET SDK 8.0](https://dotnet.microsoft.com/download/dotnet/current) or later.

## Usage

- List benchmarks 

```
dotnet run -c Release --project ./src --list tree
```

- Run and choose your benchmarks after

```
dotnet run -c Release --project ./src
```

- Run all benchmarks

```
dotnet run -c Release --project ./src --filter *
```

- Run benchmarks with filter

```
dotnet run -c Release --project ./src --filter benchmark.Samples.Linq*
```

## Plots

https://benchmarkdotnet.org/articles/configs/exporters.html#plots
>[You can install R to automatically](https://www.r-project.org/) get nice plots of your benchmark results. First, make sure `Rscript.exe` or Rscript is in your path, or define an R_HOME environment variable pointing to the R installation directory.
eg: If Rscript is located in `/path/to/R/R-1.2.3/bin/Rscript`, then  `R_HOME` must point to `/path/to/R/R-1.2.3/`, it should not point to `/path/to/R/R-1.2.3/bin`
